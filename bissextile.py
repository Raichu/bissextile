#!/usr/bin/env python3
# -*-coding:utf-8 -*

"""Ce programme permet de vérifier si une année est bissextile ou non"""

from math import floor #importe la fonction partie entière floor() depuis la bibliothèque math

try:
    année=int(input('En quelle année sommes-nous ? ')) #demande l'année et la stocke dans la variable année
    assert année > 0 #vérifie si l'année est bien strictement positive
except ValueError:
    année=int(input("Veuillez saisir une année correcte, c'est-à-dire un nombre entier : ")) #message d'erreur en cas d'année qui n'est pas un nombre convertible en entier
except AssertionError:
    année=int(input("Veuillez saisir une année strictement positive : ")) #autre message d'erreur en cas d'année négative ou nulle

#si l'année est divisible par 4
if année/4 == floor(année/4): #si l'année est divisible par 4
    if année/100 == floor(année/100): #si l'année est divisible par 100
        if année/400 == floor(année/400): #si l'année est divisible par 400
            print("oui l'année est bissextile") #année divisible par 400 = bissextile
        else:
            print("non l'année n'est pas bissextile") #année divisible par 100 mais pas 400
    else:
        print("oui l'année est bissextile") #année divisible par 4 mais pas par 100
else:
    print("non l'année n'est pas bissextile") #année non divisible par 4
