#!/usr/bin/env python3
# -*-coding:Latin-1 -*

"""Ce programme permet de verifier si une annee est bissextile ou non"""

from math import floor #importe la fonction partie entière floor() depuis la bibliothèque math
import os #importe les fonctions windows dont une permettra de mettre pause a la fin du programme

annee=int(input('En quelle annee sommes-nous ?')) #demande l'annee et la stocke dans la variable annee

#si l'annee est divisible par 4
if annee/4 == floor(annee/4): #si l'annee est divisible par 4
    if annee/100 == floor(annee/100): #si l'annee est divisible par 100
        if annee/400 == floor(annee/400): #si l'annee est divisible par 400
            print("oui l'annee est bissextile") #annee divisible par 400 = bissextile
        else:
            print("non l'annee n'est pas bissextile") #annee divisible par 100 mais pas 400
    else:
        print("oui l'annee est bissextile") #annee divisible par 4 mais pas par 100
else:
    print("non l'annee n'est pas bissextile") #annee non divisible par 4

os.system("pause") #pour voir le resultat sans que le programme quitte
